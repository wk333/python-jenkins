Name:		python-jenkins
Version:	1.7.0
Release:	3
Summary:	Python bindings for the remote Jenkins API
License:	BSD
URL:		http://git.openstack.org/cgit/openstack/python-jenkins
Source0:	https://files.pythonhosted.org/packages/85/8e/52223d8eebe35a3d86579df49405f096105328a9d80443eaed809f6c374f/python-jenkins-1.7.0.tar.gz
Patch1:		bugfix_function_loss.patch

BuildRequires:  python3-sphinx
BuildArch:      noarch

%description
Python Jenkins is a library for the remote API of the Jenkins continuous
integration server. It is useful for creating and managing jobs as well as
build nodes.


%package -n python3-jenkins
Summary:        %{summary}
BuildRequires:  python3-devel
BuildRequires:  python3-kerberos
BuildRequires:  python3-mock
BuildRequires:  python3-multi_key_dict
BuildRequires:  python3-pbr >= 0.8.2
BuildRequires:  python3-requests
BuildRequires:  python3-requests-mock
BuildRequires:  python3-setuptools
BuildRequires:  python3-six >= 1.3.0
BuildRequires:  python3-testscenarios
BuildRequires:  python3-testtools
%{?python_provide:%python_provide python3-jenkins}

%if %{undefined __pythondist_requires}
Requires:       python3-multi_key_dict
Requires:       python3-pbr >= 0.8.2
Requires:       python3-requests
Requires:       python3-six >= 1.3.0
%endif

Recommends:     python3-kerberos

%description -n python3-jenkins
Python Jenkins is a library for the remote API of the Jenkins continuous
integration server. It is useful for creating and managing jobs as well as
build nodes.


%prep
%autosetup -n python-jenkins-1.7.0 -p1

# Remove env from __init__.py
sed -i '1{s|^#!/usr/bin/env python||}' jenkins/__init__.py


%build
export PBR_VERSION=%{version}

%py3_build

PYTHONDONTWRITEBYTECODE=1 \
  PYTHONPATH=$PWD \
  %make_build -C doc html man
rm doc/build/html/.buildinfo


%install
export PBR_VERSION=%{version}

%py3_install

install -D -m0644 -p doc/build/man/pythonjenkins.1 %{buildroot}%{_mandir}/man1/pythonjenkins.1


%check
%{__python3} -m testtools.run discover tests


%files -n python3-jenkins
%doc README.rst doc/build/html
%license COPYING
%{python3_sitelib}/jenkins/
%{python3_sitelib}/python_jenkins-%{version}-py%{python3_version}.egg-info/
%{_mandir}/man1/pythonjenkins.1.*


%changelog
* Tue Jun 28 2022 wangkai <wangkai385@h-partners.com> -1.7.0-3
- Remove buildrequires python-nose

* Fri Aug 20 2021 Pengju Jiang <jiangpengju2@huawei.com> - 1.7.0-2
- bugfix_function_loss.patch

* Tue Jul 27 2021 Python_Bot <Python_Bot@openeuler.org> - 1.7.0-1
- Package Spec generated
